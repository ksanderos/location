package ksanderos.com.ui.details;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import ksanderos.com.data.db.model.Place;


public interface DetailsMvpView extends MvpView {
    void show(Place item);
    void showPlaceMarker(MarkerOptions markerOptions);
    void showMap();
    void zoomMap(CameraUpdate cameraUpdate);
}
