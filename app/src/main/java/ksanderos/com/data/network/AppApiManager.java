package ksanderos.com.data.network;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import ksanderos.com.data.db.model.NearbySearch;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AppApiManager implements ApiManager {

//    private Retrofit retrofit;
    private ApiRequest service;

    public AppApiManager() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://maps.googleapis.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        service = retrofit.create(ApiRequest.class);
    }

    @Override
    public void sendRequestNearbySearch(String location, int radius, String key, Callback<NearbySearch> callbackNearbySearch) {
        Call<NearbySearch> request = service.getNearbySearch(location, radius, key);
        request.enqueue(callbackNearbySearch);
    }

}
