package ksanderos.com.data.db;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import ksanderos.com.data.db.model.NearbySearch;
import ksanderos.com.data.db.model.Place;


public class AppDbManager implements DbManager {

    private Realm realm;

    @Override
    public void openDatabase() {
        realm = Realm.getDefaultInstance();
    }

    @Override
    public void closeDatabase() {
        realm.close();
    }

    @Override
    public void insertNearbySearch(NearbySearch nearbySearchData) {
        realm.beginTransaction();
        realm.deleteAll();
        realm.insertOrUpdate(nearbySearchData);
        realm.commitTransaction();
    }

    @Override
    public List<Place> getPlaces() {
        realm.beginTransaction();
        RealmResults<Place> resultsRealm = realm.where(Place.class).findAll();
        List<Place> resultsCopy = realm.copyFromRealm(resultsRealm);
        realm.commitTransaction();
        return resultsCopy;
    }
}
