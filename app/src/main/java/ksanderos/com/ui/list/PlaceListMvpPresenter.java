package ksanderos.com.ui.list;


import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface PlaceListMvpPresenter<MvpView> extends MvpPresenter<PlaceListMvpView> {
    void loadPlaces();

    void downloadPlaces();
}
