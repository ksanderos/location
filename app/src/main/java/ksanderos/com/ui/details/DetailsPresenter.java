package ksanderos.com.ui.details;

import android.support.annotation.NonNull;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import ksanderos.com.data.AppDataManager;
import ksanderos.com.data.db.model.Place;

import static ksanderos.com.utils.C.ZOOM;

public class DetailsPresenter extends MvpBasePresenter<DetailsMvpView> {

    void showInfo() {
        final Place place = AppDataManager.getInstance().getCachePlace();
        if (place != null) {
            ifViewAttached(false, new ViewAction<DetailsMvpView>() {
                @Override
                public void run(@NonNull DetailsMvpView view) {
                    view.show(place);
                }
            });
        }
    }

    void onMapReady() {
        final Place place = AppDataManager.getInstance().getCachePlace();
        final LatLng placeLocation = new LatLng(place.getGeometry().getLocation().getLat(), place.getGeometry().getLocation().getLng());
        ifViewAttached(false, new ViewAction<DetailsMvpView>() {
            @Override
            public void run(@NonNull DetailsMvpView view) {
                view.showMap();
                view.showPlaceMarker(new MarkerOptions().position(placeLocation).title(place.getName()));
                view.zoomMap(CameraUpdateFactory.newLatLngZoom(placeLocation, ZOOM));
            }
        });

    }
}
