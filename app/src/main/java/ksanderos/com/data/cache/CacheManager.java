package ksanderos.com.data.cache;

import ksanderos.com.data.db.model.Place;


public interface CacheManager {
    void setCachePlace(Place place);

    Place getCachePlace();

}
