package ksanderos.com.ui.main;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import ksanderos.com.data.db.model.Place;
import ksanderos.com.location.R;
import ksanderos.com.ui.details.DetailsFragment;
import ksanderos.com.ui.list.PlaceListFragment;
import ksanderos.com.ui.list.PlacesRecyclerAdapter;

public class MainActivity extends AppCompatActivity
        implements MainMvpView, View.OnClickListener, PlacesRecyclerAdapter.OnClickListenerPlaceItem {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.loadingView)
    ProgressBar progressBar;

    private PlaceListFragment placeListFragment;
    private DetailsFragment detailFragment;
    private MainPresenter mainPresenter;

    void init() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        getSupportActionBar().setHomeButtonEnabled(true);

        toolbar.setNavigationOnClickListener(this);
        toolbar.setTitle(R.string.app_name);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        init();

        placeListFragment = new PlaceListFragment();
        detailFragment = new DetailsFragment();

        mainPresenter = new MainPresenter();
        mainPresenter.attachView(this);
        mainPresenter.downloadDataAndShowPlaceListFragment();
        mainPresenter.showSubTitle();
    }

    @Override
    public void showListFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        fragmentTransaction.replace(R.id.fragment_container, placeListFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void showDetailsFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        fragmentTransaction.replace(R.id.fragment_container, detailFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onClick(View view) {
        mainPresenter.onClickBack();
    }

    @Override
    public void onClickPlace(Place place) {
        mainPresenter.onClickPlace(place);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mainPresenter.detachView();
    }

    @Override
    public void showSubTitle(int resString) {
        toolbar.setSubtitle(resString);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mainPresenter.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        mainPresenter.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onBackPressed() {
        mainPresenter.onClickBack();
    }

    @Override
    public void close() {
       System.exit(0);
    }
}
