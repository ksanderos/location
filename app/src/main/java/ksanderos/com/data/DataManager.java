package ksanderos.com.data;

import ksanderos.com.data.cache.CacheManager;
import ksanderos.com.data.db.DbManager;
import ksanderos.com.data.network.ApiManager;

public interface DataManager extends DbManager, ApiManager, CacheManager {

}
