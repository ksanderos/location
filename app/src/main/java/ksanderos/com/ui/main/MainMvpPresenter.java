package ksanderos.com.ui.main;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface MainMvpPresenter extends MvpPresenter<MainMvpView> {

    void onClickBack();

}
