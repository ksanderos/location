package ksanderos.com.ui.main;


import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface MainMvpView extends MvpView {
    void showSubTitle(int resString);

    void showListFragment();

    void showDetailsFragment();

    void showLoading();

    void hideLoading();

    void close();
}
