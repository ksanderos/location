package ksanderos.com.ui.details;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ksanderos.com.data.db.model.Place;
import ksanderos.com.location.R;
import ksanderos.com.ui.list.PlacesRecyclerAdapter;
import ksanderos.com.ui.list.PlacesRecyclerAdapter.PlaceViewHolder;


public class DetailsFragment extends MvpFragment<DetailsMvpView, DetailsPresenter>
        implements DetailsMvpView, OnMapReadyCallback {

    @BindView(R.id.mapView)
    MapView mapView;

    PlacesRecyclerAdapter.PlaceViewHolder placeViewHolder;

    private GoogleMap googleMap;
    private Unbinder unbinder;


        @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }
    @NonNull
    @Override
    public DetailsPresenter createPresenter() {
        return new DetailsPresenter();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details, container, false);
        unbinder = ButterKnife.bind(this, view);
        presenter.attachView(this);
        placeViewHolder = new PlaceViewHolder(view.findViewById(R.id.include_place_item));
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
    }

    @OnClick(R.id.include_place_item)
    public void onClickPlaceItem() {
        Toast.makeText(getContext(), "Click Place", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void show(Place item) {
        placeViewHolder.setAdress(item.getVicinity());
        placeViewHolder.setName(item.getName());
        placeViewHolder.setIconImage(item.getIcon());
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        presenter.onMapReady();
    }

    @Override
    public void showMap() {
        googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        googleMap.setBuildingsEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
    }

    @Override
    public void showPlaceMarker(MarkerOptions markerOptions) {
        googleMap.addMarker(markerOptions);
    }

    @Override
    public void zoomMap(CameraUpdate cameraUpdate) {
        googleMap.moveCamera(cameraUpdate);
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
        presenter.showInfo();
    }

    @Override
    public void onPause() {
        mapView.onResume();
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onDestroy() {
        mapView.onDestroy();
        presenter.detachView();
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

}
