package ksanderos.com.ui.list;

import android.support.annotation.NonNull;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.List;

import javax.annotation.Nonnull;

import ksanderos.com.data.AppDataManager;
import ksanderos.com.data.db.model.NearbySearch;
import ksanderos.com.data.db.model.Place;
import ksanderos.com.location.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static ksanderos.com.utils.C.KEY;
import static ksanderos.com.utils.C.LOCATION;
import static ksanderos.com.utils.C.RADIUS;

public class PlaceListPresenter extends MvpBasePresenter<PlaceListMvpView>
        implements PlaceListMvpPresenter<PlaceListMvpView>, Callback<NearbySearch> {

    @Override
    public void loadPlaces() {

        ifViewAttached(new ViewAction<PlaceListMvpView>() {
            @Override
            public void run(@NonNull PlaceListMvpView view) {
                view.showLoading();
                List<Place> places = AppDataManager.getInstance().getPlaces();
                if (!places.isEmpty()) {
                    view.showPlaces(places);
                    view.showMessage(R.string.database_read_sucess);

                } else {
                    view.showMessage(R.string.database_read_fail);
                }
                view.hideLoading();

            }
        });

    }

    @Override
    public void downloadPlaces() {
        ifViewAttached(true, new ViewAction<PlaceListMvpView>() {
            @Override
            public void run(@NonNull PlaceListMvpView view) {
                view.showLoading();
                view.showMessage(R.string.api_download);
                AppDataManager.getInstance().sendRequestNearbySearch(LOCATION, RADIUS, KEY, PlaceListPresenter.this);
            }
        });
    }


    @Override
    public void onResponse(@Nonnull Call<NearbySearch> request, @Nonnull final Response<NearbySearch> response) {
        ifViewAttached(true, new ViewAction<PlaceListMvpView>() {
            @Override
            public void run(@NonNull PlaceListMvpView view) {
                if (response.isSuccessful()) {
                    view.showMessage(R.string.insert_data);
                    AppDataManager.getInstance().insertNearbySearch((response.body()));
                } else {
                    view.showError(R.string.respone_error);
                }
                view.showPlaces(AppDataManager.getInstance().getPlaces());
                view.showMessage(R.string.download_insert_and_load_to_database);
                view.hideLoading();
            }
        });
    }

    @Override
    public void onFailure(@Nonnull Call<NearbySearch> request, Throwable t) {
        ifViewAttached(true, new ViewAction<PlaceListMvpView>() {
            @Override
            public void run(@NonNull PlaceListMvpView view) {
                loadPlaces();
                view.showError(R.string.respone_error);
                view.hideLoading();

            }
        });
    }

}
