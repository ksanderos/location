package ksanderos.com.ui.main;

import android.os.Bundle;
import android.util.Log;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import ksanderos.com.data.AppDataManager;
import ksanderos.com.data.db.model.NearbySearch;
import ksanderos.com.data.db.model.Place;
import ksanderos.com.location.R;
import ksanderos.com.utils.C;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static ksanderos.com.utils.C.DETAIL_FRAGMENT;
import static ksanderos.com.utils.C.KEY;
import static ksanderos.com.utils.C.KEY_FRAGMENT;
import static ksanderos.com.utils.C.LOCATION;
import static ksanderos.com.utils.C.PLACE_LIST_FRAGMENT;
import static ksanderos.com.utils.C.RADIUS;
import static ksanderos.com.utils.C.STARTED;

public class MainPresenter extends MvpBasePresenter<MainMvpView>
        implements MainMvpPresenter {
    private static int SELECTED_FRAGMENT = STARTED;

    public void showSubTitle() {
        switch (SELECTED_FRAGMENT) {
            case DETAIL_FRAGMENT:
                getView().showSubTitle(R.string.sub_title_detailsFragment);
                break;
            case PLACE_LIST_FRAGMENT:
                getView().showSubTitle(R.string.sub_title_listFragment);
                break;
        }
    }

    @Override
    public void onClickBack() {
        if (SELECTED_FRAGMENT == DETAIL_FRAGMENT) {
            SELECTED_FRAGMENT = PLACE_LIST_FRAGMENT;
            getView().showListFragment();
            getView().showSubTitle(R.string.sub_title_listFragment);
        } else {
            getView().close();
        }

    }

    public void onClickPlace(Place place) {
        AppDataManager.getInstance().setCachePlace(place);
        SELECTED_FRAGMENT = DETAIL_FRAGMENT;
        getView().showDetailsFragment();
        getView().showSubTitle(R.string.sub_title_detailsFragment);
    }

    void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            SELECTED_FRAGMENT = savedInstanceState.getInt(KEY_FRAGMENT);
        }
    }

    void onSaveInstanceState(Bundle outState) {
        if (outState != null) {
            outState.putInt(KEY_FRAGMENT, SELECTED_FRAGMENT);
        }
    }

    void showPlaceListFragmentOnStartActivity() {
        if (SELECTED_FRAGMENT == STARTED) {
            SELECTED_FRAGMENT = PLACE_LIST_FRAGMENT;
            getView().showListFragment();
            getView().showSubTitle(R.string.sub_title_listFragment);

        }
    }

    void downloadDataAndShowPlaceListFragment() {
        getView().showLoading();
        AppDataManager.getInstance().sendRequestNearbySearch(LOCATION, RADIUS, KEY, new Callback<NearbySearch>() {
            @Override
            public void onResponse(Call<NearbySearch> call, Response<NearbySearch> response) {
                if (response.isSuccessful()) {
                    AppDataManager.getInstance().insertNearbySearch((response.body()));
                }
                showPlaceListFragmentOnStartActivity();
                getView().hideLoading();
            }

            @Override
            public void onFailure(Call<NearbySearch> call, Throwable t) {
                showPlaceListFragmentOnStartActivity();
                getView().hideLoading();
            }
        });
    }

}
