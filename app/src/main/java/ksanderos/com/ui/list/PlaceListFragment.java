package ksanderos.com.ui.list;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hannesdorfmann.mosby3.mvp.MvpFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ksanderos.com.data.db.model.Place;
import ksanderos.com.location.R;

public class PlaceListFragment
        extends MvpFragment<PlaceListMvpView, PlaceListPresenter>
        implements PlaceListMvpView {

    @BindView(R.id.fab)
    FloatingActionButton floatingActionButton;

    @BindView(R.id.recyclerViewPlace)
    RecyclerView recyclerView;

    @BindView(R.id.loadingView)
    ProgressBar progressBar;

    @BindView(R.id.errorView)
    TextView textView;

    private PlacesRecyclerAdapter placesRecyclerAdapter;
    private PlacesRecyclerAdapter.OnClickListenerPlaceItem onClickListenerPlaceItem;
    private Unbinder unbinder;

    public PlaceListFragment() {
    }

    @NonNull
    @Override
    public PlaceListPresenter createPresenter() {
        return new PlaceListPresenter();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        initRecycler();
        presenter.attachView(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.loadPlaces();
    }

    private void initRecycler() {
        placesRecyclerAdapter = new PlacesRecyclerAdapter(onClickListenerPlaceItem);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(placesRecyclerAdapter);
    }


    @OnClick(R.id.fab)
    public void onClickFloatingButton() {
        presenter.downloadPlaces();
    }

    @Override
    public void showLoading() {
        textView.setVisibility(View.GONE);
        recyclerView.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showError(int msg) {
        textView.setVisibility(View.VISIBLE);
        textView.setText(msg);
    }

    @Override
    public void showPlaces(List<Place> list) {
        placesRecyclerAdapter.setPlaces(list);
        placesRecyclerAdapter.notifyDataSetChanged();
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showMessage(int message) {
        textView.setVisibility(View.VISIBLE);
        textView.setText(message);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            onClickListenerPlaceItem = (PlacesRecyclerAdapter.OnClickListenerPlaceItem) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement PlacesRecyclerAdapter.OnClickListenerPlaceItem");
        }
    }

    @Override
    public void onDestroy() {
        presenter.detachView();
        super.onDestroy();
        unbinder.unbind();
    }

}
