package ksanderos.com.data.network;

import ksanderos.com.data.db.model.NearbySearch;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiRequest {

    @GET("maps/api/place/nearbysearch/json?")
    Call<NearbySearch> getNearbySearch(@Query("location") String location, @Query("radius") int radius , @Query("key") String key);
}
