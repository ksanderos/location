package ksanderos.com.data.network;

import ksanderos.com.data.db.model.NearbySearch;
import retrofit2.Callback;

public interface ApiManager {
    void sendRequestNearbySearch(String location, int radius, String key, Callback<NearbySearch> callbackNearbySearch);

}
