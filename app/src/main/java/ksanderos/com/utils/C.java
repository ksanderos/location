package ksanderos.com.utils;

import android.os.Environment;


public class C {
    public static final String KEY = "AIzaSyBpM6MnCsvi7B47K53Bac2jxmV4tR3AQIQ";
    public static final String LOCATION = "52,21";
    public static final int RADIUS = 1500;

    public static final int ZOOM = 15;
    public static final String TAG_LOCATION = "TAG_LOCATION";

    public static final String KEY_FRAGMENT = "SELECTED_FRAGMENT";

    public static final int STARTED = 100;
    public static final int PLACE_LIST_FRAGMENT = 0;
    public static final int DETAIL_FRAGMENT = 1;


}
