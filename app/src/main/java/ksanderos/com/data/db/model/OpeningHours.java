package ksanderos.com.data.db.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

public class OpeningHours extends RealmObject {

    @SerializedName("open_now")
    @Expose
    private Boolean openNow;
    @SerializedName("weekday_text")
    @Expose
    private RealmList<String> weekdayText = null;

    public Boolean getOpenNow() {
        return openNow;
    }

    public void setOpenNow(Boolean openNow) {
        this.openNow = openNow;
    }

    public RealmList<String> getWeekdayText() {
        return weekdayText;
    }

    public void setWeekdayText(RealmList<String> weekdayText) {
        this.weekdayText = weekdayText;
    }

}