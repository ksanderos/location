package ksanderos.com.data;

import java.util.List;

import ksanderos.com.data.cache.AppCacheManager;
import ksanderos.com.data.db.AppDbManager;
import ksanderos.com.data.db.model.NearbySearch;
import ksanderos.com.data.db.model.Place;
import ksanderos.com.data.network.AppApiManager;
import retrofit2.Callback;

public class AppDataManager implements DataManager {
    private static AppDataManager appDataManager;

    private final AppDbManager appDbManager;
    private final AppApiManager appApiManager;
    private final AppCacheManager appCacheManager;

    private AppDataManager() {
        appDbManager = new AppDbManager();
        appApiManager = new AppApiManager();
        appCacheManager = new AppCacheManager();
    }

    public static AppDataManager getInstance() {
        if (appDataManager == null) {
            appDataManager = new AppDataManager();
        }
        return appDataManager;
    }

    @Override
    public void openDatabase() {
        appDbManager.openDatabase();
    }

    @Override
    public void closeDatabase() {
        appDbManager.closeDatabase();
    }

    @Override
    public void insertNearbySearch(NearbySearch nearbySearchData) {
        appDbManager.insertNearbySearch(nearbySearchData);
    }

    @Override
    public List<Place> getPlaces() {
        return appDbManager.getPlaces();
    }

    @Override
    public void sendRequestNearbySearch(String location, int radius, String key, Callback<NearbySearch> callbackNearbySearch) {
        appApiManager.sendRequestNearbySearch(location, radius, key, callbackNearbySearch);
    }

    @Override
    public void setCachePlace(Place place) {
        appCacheManager.setCachePlace(place);
    }

    @Override
    public Place getCachePlace() {
        return appCacheManager.getCachePlace();
    }
}
