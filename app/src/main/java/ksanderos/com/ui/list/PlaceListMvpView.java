package ksanderos.com.ui.list;

import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.List;

import ksanderos.com.data.db.model.Place;

public interface PlaceListMvpView extends MvpView {

    void showLoading();

    void hideLoading();

    void showError(int msg);

    void showPlaces(List<Place> list);

    void showMessage(int resMsg);

}
