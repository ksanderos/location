package ksanderos.com;

import android.app.Application;
import com.facebook.drawee.backends.pipeline.Fresco;

import io.realm.Realm;
import ksanderos.com.data.AppDataManager;

public class LocationApplication extends Application
{
    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
        Realm.init(this);
        AppDataManager.getInstance().openDatabase();
    }
}
