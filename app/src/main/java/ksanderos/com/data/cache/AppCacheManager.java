package ksanderos.com.data.cache;

import com.google.android.gms.maps.model.LatLng;

import ksanderos.com.data.db.model.Place;

public class AppCacheManager implements CacheManager {
    private Place place;

    @Override
    public void setCachePlace(Place place) {
        this.place = place;
    }

    @Override
    public Place getCachePlace() {
        return place;
    }

}
