package ksanderos.com.ui.list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ksanderos.com.data.db.model.Place;
import ksanderos.com.location.R;

public class PlacesRecyclerAdapter extends RecyclerView.Adapter<PlacesRecyclerAdapter.PlaceViewHolder> {

    private List<Place> placeList;
    private OnClickListenerPlaceItem onClickListenerPlaceItem;

    public PlacesRecyclerAdapter(OnClickListenerPlaceItem onClickListenerPlaceItem) {
        this.onClickListenerPlaceItem = onClickListenerPlaceItem;
    }

    public void setPlaces(List<Place> places) {
        placeList = places;
    }

    @Override
    public PlacesRecyclerAdapter.PlaceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.place_item, parent, false);
        return new PlaceViewHolder(view, onClickListenerPlaceItem);
    }

    @Override
    public void onBindViewHolder(PlacesRecyclerAdapter.PlaceViewHolder holder, int position) {
        Place place = placeList.get(position);
        holder.setName(place.getName());
        holder.setAdress(place.getVicinity());
        holder.setIconImage(place.getIcon());
        holder.setOnClickItem(place);
    }

    @Override
    public int getItemCount() {
        if (placeList == null) {
            return 0;
        } else {
            return placeList.size();
        }
    }

    public interface OnClickListenerPlaceItem {
        void onClickPlace(Place item);
    }

    public static class PlaceViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textViewName)
        TextView nameView;
        @BindView(R.id.textViewAdress)
        TextView adressView;
        @BindView(R.id.imageView)
        SimpleDraweeView iconImage;

        private View view;
        private OnClickListenerPlaceItem onClickListenerPlaceItem;

        private PlaceViewHolder(View view, OnClickListenerPlaceItem onClickListenerPlaceItem) {
            super(view);
            ButterKnife.bind(this, view);
            this.view = view;
            this.onClickListenerPlaceItem = onClickListenerPlaceItem;
        }

        public PlaceViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void setName(String name) {
            nameView.setText(name);
        }

        public void setAdress(String adress) {
            adressView.setText(adress);
        }

        /**
         * http://frescolib.org/docs/caching.html
         */
        public void setIconImage(String uri) {
            iconImage.setImageURI(uri);
        }

        private void setOnClickItem(final Place place) {
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickListenerPlaceItem.onClickPlace(place);
                }
            });
        }

    }
}
