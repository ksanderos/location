package ksanderos.com.data.db;

import java.util.List;

import ksanderos.com.data.db.model.NearbySearch;
import ksanderos.com.data.db.model.Place;


public interface DbManager {
    void openDatabase();

    void closeDatabase();

    void insertNearbySearch(NearbySearch nearbySearchData);

    List<Place> getPlaces();
}
